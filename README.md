# clock_server

## Overview

Clock message publisher.

## Quickstart

Execute the following command to publish a clock at *100Hz*.

    roscore &
    rosrun clock_server clock_server

## Node

### clock_server

#### Published topics

clock (rosgraph_msgs/Clock)
: the clock message

#### Parameters

~frequency (double, default: 100.0)
: the publication frequency [Hz]

~rate (double, default: 1.0)
: speed factor of the clock (>1 to "speedup time") [s/s]

~offset (double, default: 0.0)
: the initial time [s]
