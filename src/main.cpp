#include <thread>
#include <chrono>
#include <ros/ros.h>
#include <rosgraph_msgs/Clock.h>

using std::chrono::high_resolution_clock;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "clock_server");

    ros::NodeHandle nh;
    ros::NodeHandle pn("~");

    double frequency = 100.0;
    double rate = 1.0;
    double offset = 0.0;

    pn.param<double>("frequency", frequency, frequency);
    pn.param<double>("rate", rate, rate);
    pn.param<double>("offset", offset, offset);

    ros::Publisher clock_pub = nh.advertise<rosgraph_msgs::Clock>("clock", 1, true);
    std::chrono::nanoseconds sleep_duration((unsigned long)(1e9 / frequency));
    high_resolution_clock::time_point now, next_wake_up; high_resolution_clock::time_point start_time = high_resolution_clock::now();
    std::chrono::duration<double> elapsed_time;
    rosgraph_msgs::Clock msg;

    while (ros::ok())
    {
        now = high_resolution_clock::now();
        elapsed_time = now - start_time;
        next_wake_up = now + sleep_duration;
        msg.clock.fromSec(offset + elapsed_time.count() * rate);
        clock_pub.publish(msg);
        std::this_thread::sleep_until(next_wake_up);
    }

    return 0;
}
